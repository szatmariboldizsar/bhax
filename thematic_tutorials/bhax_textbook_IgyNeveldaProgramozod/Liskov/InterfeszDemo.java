interface Interfesz
{
	void abstractMethod();    

	default void defaultMethod()
	{
		System.out.println("Ez egy default metódus");
	}

	static void staticMethod()
	{
		System.out.println("Ez egy statikus metódus");
	}
}

class InterfeszDefault implements Interfesz {

	@Override
	public void abstractMethod() 
	{
		System.out.println("Ez egy implementált absztrakt metódus");
	}

	@Override
	public void defaultMethod() 
	{
		System.out.println("Ez egy felüldefiniált default metódus");
	}
}

public class InterfeszDemo implements Interfesz {
	
	@Override
	public void abstractMethod() 
	{
		System.out.println("Ez egy implementált absztrakt metódus");
	}
	
	/*
	@Override
	static void staticMethod()
	{
		System.out.println("Ez egy statikus metódus");
	}
	Nem felüldefiniálható*/
	
	public static void main(String[] args)
	{

		InterfeszDemo interfeszDemo = new InterfeszDemo();
		
		InterfeszDefault interfeszDefault = new InterfeszDefault();

		interfeszDemo.abstractMethod();

		interfeszDemo.defaultMethod();
		interfeszDefault.defaultMethod();

		//interfeszDemo.staticMethod(); nem használható

		Interfesz.staticMethod(); //csak így hívható meg
	}
}
