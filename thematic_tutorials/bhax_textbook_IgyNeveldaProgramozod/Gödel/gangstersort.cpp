#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector<int> numbers = {6, -2, 4, -5, 5};

	for(auto& i : numbers){
		cout << i << " ";
	}
	cout << endl << endl;

	sort(numbers.begin(), numbers.end(), [](int a, int b){ return a < b;});
	for(auto& i : numbers){
		cout << i << " ";
	}
	cout << endl;
}
