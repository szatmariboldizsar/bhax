#include <stdio.h>

int main()
{
	int a = 3;
	int b = 5;

	printf("a = %d\nb = %d\nFelcserélés...\n", a, b);

	a ^= b;
	b ^= a;
	a ^= b;

	printf("a = %d\nb = %d\n", a, b);
}
