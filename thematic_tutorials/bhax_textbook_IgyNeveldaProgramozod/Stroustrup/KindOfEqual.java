public class KindOfEqual
{
	public static void main(String[] args)
	{
		String first = "...";
		String second = "...";
		String third = new String("...");

		var firstMatchesSecondWithEquals = first.equals(second);
		var firstMatchesSecondWithEqualToOperator = first == second;
		var firstMatchesThirdWithEquals = first.equals(third);
		var firstMatchesThirdWithEqualToOperator = first == third;

		System.out.println(firstMatchesSecondWithEquals);
		System.out.println(firstMatchesSecondWithEqualToOperator);
		System.out.println(firstMatchesThirdWithEquals);
		System.out.println(firstMatchesThirdWithEqualToOperator);
	}
}
