import java.util.Arrays;

public class IntegerStorage {
	
	public static int[] add(int[] array, Integer value)
	{
		int[] tmparray = Arrays.copyOf(array, array.length + 1);
		
		tmparray[tmparray.length - 1] = value;
		
		return tmparray;
		
	}

	public static boolean contains(int[] array, Integer value)
	{
		boolean contains = false;
		int min = 0;
		int max = array.length - 1;
		
		while (!contains && min <= max)
		{
			int mid = (min + max)/2;
			
			if (array[mid] == value)
			{
				contains = true;
			}
			else if (array[mid] < value)
			{
				min = mid + 1;
			}
			else if (array[mid] > value)
			{
				max = mid - 1;
			}
		}
		return contains;
	}
	
	public static int[] bubbleSort (int[] array)
	{
		int tmp;
		int length = array.length;
		for (int i = 0; i < length-1; i++)
		{
			for (int j = 0; j < length-i-1; j++)
			{
				if (array[j] > array[j+1]) 
				{
					tmp = array[j];
					array[j] = array[j+1];
					array[j+1] = tmp;
				}
			} 
		}
		return array;
	}
	
	public static void main(String[] args) {
		
		int[] Array = {2, 4, 3, 1, 5, 7, 6, 9};
		
		for (int i:Array)
		{
			System.out.print(i + " ");
		}
		
		System.out.println("\n");
		Array = add(Array, 8);
		
		for (int i:Array)
		{
			System.out.print(i + " ");
		}
		
		System.out.println("\n");
		int[] Sorted = bubbleSort(Array);
		
		for (int i:Sorted)
		{
			System.out.print(i + " ");
		}
		
		System.out.println("\n");
		
		if (contains(Array, 9))
		{
			System.out.println("Tartalmazza");
		}
		else
		{
			System.out.println("Nem Tartalmazza");
		}

		if (contains(Array, 10))
		{
			System.out.println("Tartalmazza");
		}
		else
		{
			System.out.println("Nem Tartalmazza");
		}
		
	}

}
