<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Chomsky!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Decimálisból unárisba átváltó Turing gép</title>
        <para>
            Állapotátmenet gráfjával megadva írd meg ezt a gépet!
        </para>
        <para>
            Megoldás forrása: az első előadás <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG-BHAX/Prog1_1.pdf">27 fólia</link>.               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... ezt kell az olvasónak kidolgoznia, mint labor- vagy otthoni mérési feladatot!
            Ha mi már megtettük, akkor használd azt, dolgozd fel, javítsd, adj hozzá értéket!
        </para>
    </section>        
        
    <section>
        <title>Az a<superscript>n</superscript>b<superscript>n</superscript>c<superscript>n</superscript> nyelv nem környezetfüggetlen</title>
        <para>
            Mutass be legalább két környezetfüggő generatív grammatikát, amely ezt a nyelvet generálja!
        </para>
        <para>
            Megoldás forrása: az első előadás <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG-BHAX/Prog1_1.pdf">30-32 fólia</link>.               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... ezt kell az olvasónak kidolgoznia, mint labor- vagy otthoni mérési feladatot!
            Ha mi már megtettük, akkor használd azt, dolgozd fel, javítsd, adj hozzá értéket!
        </para>
    </section>        
                
    <section>
        <title>Hivatkozási nyelv</title>
        <para>
            A <citation>KERNIGHANRITCHIE</citation> könyv C referencia-kézikönyv/Utasítások melléklete alapján definiáld 
            BNF-ben a C utasítás fogalmát!
            Majd mutass be olyan kódcsipeteket, amelyek adott szabvánnyal nem fordulnak (például C89), mással (például C99) igen.
        </para>
        <para>
            Megoldás forrása: az első előadás <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG-BHAX/Prog1_1.pdf">63-65 fólia</link>.               
        </para>
	<para>
	C89 szabvány:
	</para>
        <programlisting language="c"><![CDATA[%{
int i;
for (i = 0; i < n; i++)
]]></programlisting>
	<para>
	C99 szabvány:
	</para>
        <programlisting language="c"><![CDATA[%{
for (int i = 0; i < n; i++)
]]></programlisting>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
	<para>
	A Kernighan-Richie könyv alapján 6 féle utasítás létezik (lehet több de a könyv csak ezekről ejt szót): címkézett utasítások, kifejezésutasítások, összetett utasítások, kiválasztó utasítások, iterációs utasítások, vezérlésátadó utasítások. Magyarázatuk megtalálható a könyvben. Az utasítások definíciója a könyv alapján:
	</para>
	<para>
	"Az utasítások a leírásuk sorrendjében hajtódnak végre, kivéve azt, ahol külön jelezzük. Az utasítások végrehajtása a hatásukban nyilvánul meg és nem rendelkeznek értékkel. Az utasítások számos csoportba sorolhatók."
	</para>
    </section>                     

    <section>
        <title>Saját lexikális elemző</title>
        <para>
            Írj olyan programot, ami számolja a bemenetén megjelenő valós számokat! 
            Nem elfogadható olyan megoldás, amely maga olvassa betűnként a bemenetet, 
            a feladat lényege, hogy lexert használjunk, azaz óriások vállán álljunk és ne kispályázzunk!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/9KnMqrkj_kU">https://youtu.be/9KnMqrkj_kU</link> (15:01-től).
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chomsky/realnumber.l">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/realnumber.l</filename>
            </link> 
        </para>
        <programlisting language="c"><![CDATA[%{
#include <stdio.h>
int realnumbers = 0;
%}
digit	[0-9]
%%
{digit}*(\.{digit}+)?	{++realnumbers; 
    printf("[realnum=%s %f]", yytext, atof(yytext));}
%%
int
main ()
{
 yylex ();
 printf("The number of real numbers is %d\n", realnumbers);
 return 0;
}
]]></programlisting>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
	<para>
	Ez a program egy lexikális elemző program, amely számolja az input-ján megjelenő valós számokat, és ki is írja azokat. A program elején deklarálunk egy db változót, ami a 0 értéket kapja, ez fogja számolni a darabszámot. Ezután megadjuk a lexernek a mintát (a valós szám mintáját). Akkor valós a szám, ha először egy számjegy fordul elő akárhányszor, azután pedig egy tizedes pont, utána pedig legalább egy számjegy. A printf függvény segítségével kiíratjuk ezeket, az atof segítségével pedig átalakítjuk a stringet double-é. A program végén meghívjuk a lexert és kiíratjuk a darabszámot.
	</para>
    </section>                     

    <section>
        <title>Leetspeak</title>
        <para>
            Lexelj össze egy l33t ciphert!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/06C_PqDpD_k">https://youtu.be/06C_PqDpD_k</link>.
        </para>
	<para>
	Videó a program futtatásáról: <link xlink:href="https://youtu.be/bN0cKa7YrJw">https://youtu.be/bN0cKa7YrJw</link>.
	</para>
        <para>
            Megoldás forrása: <link xlink:href="Chomsky/l337d1c7.l">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/l337d1c7.l</filename>
            </link>  
        </para>
        <programlisting language="c"><![CDATA[%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #include <ctype.h>

  #define L337SIZE (sizeof l337d1c7 / sizeof (struct cipher))
    
  struct cipher {
    char c;
    char *leet[4];
  } l337d1c7 [] = {

  {'a', {"4", "4", "@", "/-\\"}},
  {'b', {"b", "8", "|3", "|}"}},
  {'c', {"c", "(", "<", "{"}},
  {'d', {"d", "|)", "|]", "|}"}},
  {'e', {"3", "3", "3", "3"}},
  {'f', {"f", "|=", "ph", "|#"}},
  {'g', {"g", "6", "[", "[+"}},
  {'h', {"h", "4", "|-|", "[-]"}},
  {'i', {"1", "1", "|", "!"}},
  {'j', {"j", "7", "_|", "_/"}},
  {'k', {"k", "|<", "1<", "|{"}},
  {'l', {"l", "1", "|", "|_"}},
  {'m', {"m", "44", "(V)", "|\\/|"}},
  {'n', {"n", "|\\|", "/\\/", "/V"}},
  {'o', {"0", "0", "()", "[]"}},
  {'p', {"p", "/o", "|D", "|o"}},
  {'q', {"q", "9", "O_", "(,)"}},
  {'r', {"r", "12", "12", "|2"}},
  {'s', {"s", "5", "$", "$"}},
  {'t', {"t", "7", "7", "'|'"}},
  {'u', {"u", "|_|", "(_)", "[_]"}},
  {'v', {"v", "\\/", "\\/", "\\/"}},
  {'w', {"w", "VV", "\\/\\/", "(/\\)"}},
  {'x', {"x", "%", ")(", ")("}},
  {'y', {"y", "", "", ""}},
  {'z', {"z", "2", "7_", ">_"}},
  
  {'0', {"D", "0", "D", "0"}},
  {'1', {"I", "I", "L", "L"}},
  {'2', {"Z", "Z", "Z", "e"}},
  {'3', {"E", "E", "E", "E"}},
  {'4', {"h", "h", "A", "A"}},
  {'5', {"S", "S", "S", "S"}},
  {'6', {"b", "b", "G", "G"}},
  {'7', {"T", "T", "j", "j"}},
  {'8', {"X", "X", "X", "X"}},
  {'9', {"g", "g", "j", "j"}}
  
// https://simple.wikipedia.org/wiki/Leet
  };
  
%}
%%
.	{
	  
	  int found = 0;
	  for(int i=0; i<L337SIZE; ++i)
	  {
	  
	    if(l337d1c7[i].c == tolower(*yytext))
	    {
	    
	      int r = 1+(int) (100.0*rand()/(RAND_MAX+1.0));
	    
          if(r<91)
	        printf("%s", l337d1c7[i].leet[0]);
          else if(r<95)
	        printf("%s", l337d1c7[i].leet[1]);
	      else if(r<98)
	        printf("%s", l337d1c7[i].leet[2]);
	      else 
	        printf("%s", l337d1c7[i].leet[3]);

	      found = 1;
	      break;
	    }
	    
	  }
	  
	  if(!found)
	     printf("%c", *yytext);	  
	  
	}
%%
int 
main()
{
  srand(time(NULL)+getpid());
  yylex();
  return 0;
}
]]></programlisting>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
	<para>
	A létrehozott struktúra tartalmazza a kicserélendő betűt és annak a kicserélhető másait. A lexer létrehozza a C programot, majd ezt fordítjuk és futtatjuk, az írott szöveget pedig kicseréli a l33tsp34k-es verzióra.
	</para>
    </section>                     


    <section>
        <title>A források olvasása</title>
        <para>
            Hogyan olvasod, hogyan értelmezed természetes nyelven az alábbi kódcsipeteket? Például
            <programlisting><![CDATA[if(signal(SIGINT, jelkezelo)==SIG_IGN)
    signal(SIGINT, SIG_IGN);]]></programlisting>
            Ha a SIGINT jel kezelése figyelmen kívül volt hagyva, akkor ezen túl is legyen
            figyelmen kívül hagyva, ha nem volt figyelmen kívül hagyva, akkor a jelkezelo függvény
            kezelje. (Miután a <command>man 7 signal</command> lapon megismertem a SIGINT jelet, a
            <command>man 2 signal</command> lapon pedig a használt rendszerhívást.)
        </para>

        <caution>
            <title>Bugok</title>
            <para>
                Vigyázz, sok csipet kerülendő, mert bugokat visz a kódba! Melyek ezek és miért? 
                Ha nem megy ránézésre, elkapja valamelyiket esetleg a splint vagy a frama?
            </para>
        </caution>
            
        <orderedlist numeration="lowerroman">
            <listitem>                                    
                <programlisting><![CDATA[if(signal(SIGINT, SIG_IGN)!=SIG_IGN)
    signal(SIGINT, jelkezelo);]]></programlisting>
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[for(i=0; i<5; ++i)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[for(i=0; i<5; i++)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[for(i=0; i<5; tomb[i] = i++)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[for(i=0; i<n && (*d++ = *s++); ++i)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[printf("%d %d", f(a, ++a), f(++a, a));]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[printf("%d %d", f(a), a);]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[printf("%d %d", f(&a), a);]]></programlisting>            
            </listitem>
        </orderedlist>

        <para>
            Megoldás videó: BHAX 357. adás.
        </para>

        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
            I.
        </para>
        <para>
            Ha a SIGINT jel nem lett figyelmen kívül hagyva, akkor a jelkezelo függvény kezelje. Ha figyelmen kívül lett hagyva, akkor ezen túl is legyen figyelmen kívül hagyva.
        </para>
        <para>
            II.
        </para>
        <para>
            A ciklus nem változik, ha valamit egyenlővé tennénk ++i-vel a zárójelen belül akkor a cikluson belül már i+1 lenne az értéke. A ciklus akkor fejeződik be, amikor a középső feltétel már nem teljesül.
        </para>
        <para>
            III.
        </para>
        <para>
            A for ciklus általános megadása, ezen esetben i kezdőértéke nulla, minden egyes leforgás után az értéke eggyel növekszik. A ciklus akkor fejeződik be, amikor a középső feltétel már nem teljesül.
        </para>
        <para>
            IV.
        </para>
        <para>
            A ciklus egyszer leforog, ekkor még a tömb a ciklus leforgása alatt nem módosul. Miután leforog, felveszi a már megnövelt indexű elemnek a még nem megnövelt indexet értéknek (értsd: tomb[1]=0, tomb[2]=1). Ha ++i lenne, akkor előbb megnövelné az indexet, majd felvenné értéknek az aktuális tömb elem (tomb[1]=1, tomb[2]=2). A tomb[0]-át egyik módon sem tudjuk értékkel felruházni, csak ha külön a cikluson belül, vagy előtte (vagy akár utána) megadjuk azt.
        </para>
        <para>
            V.
        </para>
        <para>
            Egy előzőleg deklarált n-ig növeljük i-t, vagy addig amíg *d++ már nem egyenlő *s++-al.
        </para>
        <para>
            VI.
        </para>
        <para>
            Kiír egy f függvény által a-ra és a+1-re visszaadott egész értéket és ugyanezt csak fordítva, az f függvényt nem ismerjük.
        </para>
        <para>
            VII.
        </para>
        <para>
            Kiír egy f függvény által a-ra visszaadott egész értéket, és az a értékét.
        </para>
        <para>
            VIII.
        </para>
        <para>
            Kiír egy f függvény által a-ra mutató mutatóra visszaadott egész értéket, és az a értékét.
        </para>
    </section>                     

    <section>
        <title>Logikus</title>
        <para>
            Hogyan olvasod természetes nyelven az alábbi Ar nyelvű formulákat?
        </para>
        <programlisting language="tex"><![CDATA[$(\forall x \exists y ((x<y)\wedge(y \text{ prím})))$ 

$(\forall x \exists y ((x<y)\wedge(y \text{ prím})\wedge(SSy \text{ prím})))$ 

$(\exists y \forall x (x \text{ prím}) \supset (x<y)) $ 

$(\exists y \forall x (y<x) \supset \neg (x \text{ prím}))$
]]></programlisting>        
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/MatLog_LaTeX">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/MatLog_LaTeX</link>
        </para>

        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/ZexiPy3ZxsA">https://youtu.be/ZexiPy3ZxsA</link>, <link xlink:href="https://youtu.be/AJSXOQFF_wk">https://youtu.be/AJSXOQFF_wk</link>
        </para>

        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
    </section>                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    <section>
        <title>Deklaráció</title>
            
        <para>
            Vezesd be egy programba (forduljon le) a következőket: 
        </para>

        <itemizedlist>
            <listitem>
                <para>egész</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutató</para>                        
            </listitem>
            <listitem>
                <para>egész referenciája</para>                        
            </listitem>
            <listitem>
                <para>egészek tömbje</para>                        
            </listitem>
            <listitem>
                <para>egészek tömbjének referenciája (nem az első elemé)</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatók tömbje</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvény</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvényre mutató mutató</para>                        
            </listitem>
            <listitem>
                <para>egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény</para>                        
            </listitem>            
            <listitem>
                <para>függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre</para>                        
            </listitem>            
        </itemizedlist>            

        <para>
            Mit vezetnek be a programba a következő nevek?
        </para>

        <itemizedlist>
            <listitem>
                <programlisting><![CDATA[int a;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *b = &a;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int &r = a;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int c[5];]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int (&tr)[5] = c;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *d[5];]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *h ();]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *(*l) ();]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int (*v (int c)) (int a, int b)]]></programlisting>            
            </listitem>            
            <listitem>
                <programlisting><![CDATA[int (*(*z) (int)) (int, int);]]></programlisting>            
            </listitem>            
        </itemizedlist>       


        <para>
            Megoldás videó: BHAX 357. adás.
        </para>
        <para>
            Az utolsó két deklarációs példa demonstrálására két olyan kódot
            írtunk, amelyek összahasonlítása azt mutatja meg, hogy miért 
            érdemes a <command>typedef</command> használata: <link xlink:href="Chomsky/fptr.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/fptr.c</filename>
            </link>,
            <link xlink:href="Chomsky/fptr2.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/fptr2.c</filename>.
            </link>  
            
        </para>
        <programlisting><![CDATA[#include <stdio.h>

int
sum (int a, int b)
{
    return a + b;
}

int
mul (int a, int b)
{
    return a * b;
}

int (*sumormul (int c)) (int a, int b)
{
    if (c)
        return mul;
    else
        return sum;

}

int
main ()
{

    int (*f) (int, int);

    f = sum;

    printf ("%d\n", f (2, 3));

    int (*(*g) (int)) (int, int);

    g = sumormul;

    f = *g (42);

    printf ("%d\n", f (2, 3));

    return 0;
}]]></programlisting>            
        <programlisting><![CDATA[#include <stdio.h>

typedef int (*F) (int, int);
typedef int (*(*G) (int)) (int, int);

int
sum (int a, int b)
{
    return a + b;
}

int
mul (int a, int b)
{
    return a * b;
}

F sumormul (int c)
{
    if (c)
        return mul;
    else
        return sum;
}

int
main ()
{

    F f = sum;

    printf ("%d\n", f (2, 3));

    G g = sumormul;

    f = *g (42);

    printf ("%d\n", f (2, 3));

    return 0;
}
]]></programlisting>            
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
	<para>
	A két programban az utolsó deklaráció van bemutatva, de a másodikban a typedef segítségével egyszerűbben olvasható a program.
	</para>
    </section>                     
    <section>
        <title>Vörös Pipacs Pokol/csiga diszkrét mozgási parancsokkal</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/Fc33ByQ6mh8">https://youtu.be/Fc33ByQ6mh8</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/nbatfai/RedFlowerHell">https://github.com/nbatfai/RedFlowerHell</link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... ezt kell az olvasónak kidolgoznia, mint labor- vagy otthoni mérési feladatot!
            Ha mi már megtettük, akkor használd azt, dolgozd fel, javítsd, adj hozzá értéket!
        </para>            
    </section>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
</chapter>                
