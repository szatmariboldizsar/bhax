package hu.unideb.prog2;

public aspect AOP {
	pointcut CallKiir() : call(void hu.unideb.prog2.LzwBinFa.kiir(*));
	after() : CallKiir() {

		System.out.println("after()");
	}

	before() : CallKiir() {

		System.out.println("before()");
	}
}
