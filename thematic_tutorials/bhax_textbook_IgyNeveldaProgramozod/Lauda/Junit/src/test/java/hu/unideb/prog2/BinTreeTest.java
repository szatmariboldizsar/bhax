package hu.unideb.prog2;

import java.io.IOException;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinTreeTest {

	@Test
	public void testMeanAndDeviationValuesAfterBinTreeRan() throws IOException {
		// Given
		double expectedMean = 4.5;
		double expectedDeviation = 1.1602387022306428;
		// When
		BinTree actual = new BinTree();
		actual.testMain();
		// Then
		assertEquals(expectedMean, actual.mean);
		assertEquals(expectedDeviation, actual.getDeviation());
	}
}
