package hu.unideb.prog2;

import java.io.*;

public class BinTree {

    static Node root        =   new Node('/');
    static Node tree        =   root;
    static int printDepth   =   0;
    static int depth        =   0;
    static int maxDepth     =   0;
    static int sumDepth     =   0;
    static int branchNum    =   0;
    static double mean      =   0;
    static double deviationSum =   0;

    static class Node {
        char value;
        Node left;
        Node right;

        Node(char value) {
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }

    static void buildTree(char value) {
        if ('0' == value) {
            if (tree.left == null) {
                tree.left = new Node(value);
                tree = root;
            } else {
                tree = tree.left;
            }
        } else if (tree.right == null) {
            tree.right = new Node(value);
            tree = root;
            } else {
                tree = tree.right;
            }
    }

    static void printTree(Node node) {
        if (node != null) {
            ++printDepth;
            printTree(node.right);
            for (int i = 0; i < printDepth; ++i)
                System.out.print("---");
            System.out.println(node.value + "(" + (printDepth - 1) + ")");
            printTree(node.left);
            --printDepth;
        }
    }

    static void getDepth(Node node) {
        if (node != null) {
            ++depth;
            getDepth(node.left);
            getDepth(node.right);
            --depth;
            if (node.right == null && node.left == null) {
                if (depth > maxDepth) {
                    maxDepth = depth;
                }
                ++branchNum;
                sumDepth += depth;
            }
        }
    }

    static void getDeviationSum(Node node) {
        if (node != null) {
            ++depth;
            getDeviationSum(node.left);
            getDeviationSum(node.right);
            --depth;
            if (node.right == null && node.left == null) {
                deviationSum += ((depth - mean) * (depth - mean));
            }
        }
    }

    static double getDeviation() {
        double deviation;
        if (branchNum - 1 > 0) {
            deviation = Math.sqrt(deviationSum / (branchNum - 1));
        } else {
            deviation = Math.sqrt(deviationSum);
        }
        return deviation;
    }

    void testMain() throws IOException {
        File           file         =   new File("input.txt");
        FileReader     fileReader   =   new FileReader(file);
        BufferedReader bufReader    =   new BufferedReader(fileReader);
        int inputChar = 0;

        while((inputChar = bufReader.read()) != -1) {
            buildTree((char) inputChar);
        }
        fileReader.close();
        getDepth(root);  //maxDepth, sumDepth, branchNum meghatározása
        depth = 0;
        mean = (double) sumDepth / branchNum;
        getDeviationSum(root);
    }

    public static void main(String[] args) throws IOException {
        File           file         =   new File("input.txt");
        FileReader     fileReader   =   new FileReader(file);
        BufferedReader bufReader    =   new BufferedReader(fileReader);
        int inputChar = 0;

        while((inputChar = bufReader.read()) != -1) {
            buildTree((char) inputChar);
        }
        fileReader.close();
        printTree(root);
        getDepth(root);  //maxDepth, sumDepth, branchNum meghatározása
        depth = 0;
        mean = (double) sumDepth / branchNum;
        getDeviationSum(root);
    }
}
